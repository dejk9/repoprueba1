import { LitElement, html } from 'lit-element';

class QuesoHeader extends LitElement {

    static get properties() {
        return {
        };
    }

    constructor() {
        super();
    }

    render() {
        return html`
            <h1>App Quesos</h1>
        `;
    }
}

customElements.define('queso-header', QuesoHeader)