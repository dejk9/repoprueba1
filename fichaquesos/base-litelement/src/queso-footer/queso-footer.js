import { LitElement, html } from 'lit-element';

class QuesoFooter extends LitElement {

    static get properties() {
        return {
        };
    }

    constructor() {
        super();
    }

    render() {
        return html`
            <h5>@Tipos de Quesos - Practica Final</h5>
        `;
    }
}

customElements.define('queso-footer', QuesoFooter)