import { LitElement, html } from 'lit-element';
import '../queso-header/queso-header.js';
import '../queso-main/queso-main.js';
import '../queso-footer/queso-footer';
import '../queso-sidebar/queso-sidebar.js';

class QuesoApp extends LitElement {

    static get properties() {
        return {
            people: {type: Array}
		    
        };
    }

    constructor() {
        super();
        
    }

    render() {
        return html`
            <queso-header></queso-header>
            <div class="row">
                <queso-sidebar class="col-2" @new-person="${this.newPerson}"></queso-sidebar>
                <queso-main class="col-10"></queso-main>
			</div>	    
            <queso-footer></queso-footer>
        `;
    }
    newPerson(e) {
        console.log("newPerson en QuesoApp");	
        this.shadowRoot.querySelector("queso-main").showPersonForm = true;
    }    

}

customElements.define('queso-app', QuesoApp)