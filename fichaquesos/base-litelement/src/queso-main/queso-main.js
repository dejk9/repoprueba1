import { LitElement, html } from 'lit-element';
import '../queso-ficha-listado/queso-ficha-listado.js';
import '../queso-form/queso-form.js';
import '../queso-info/queso-info.js';
class QuesoMain extends LitElement {
    
    static get properties() {
        return {
            people: {type: Array},
            showPersonForm: {type: Boolean}
        };
    }

    constructor() {
        super();
        this.showPersonForm = false;

        this.people = [
            /*{
                name: "Queso Panela",
                yearsInCompany: 10,
                photo: {
                    "src": "./img/quesito.jpg",
                    "alt": "Queso Panela"
                }, 
                profile: "Queso muy saludable"
            }, {
                name: "Queso Azul",
                yearsInCompany: 2,
                photo: {
                    "src": "./img/quesito.jpg",
                    "alt": "Queso Azul"
                }, 
                profile: "Queso muy fuerte"
            }, {
                name: "Queso Manchego",
                yearsInCompany: 5,
                photo: {
                    "src": "./img/quesito.jpg",
                    "alt": "Queso Manchego"
                }, 
                profile: "Queso muy grasoso, pero rico"
            }, {
                name: "Queso Mascarpone",
                yearsInCompany: 9,
                photo: {
                    "src": "./img/quesito.jpg",
                    "alt": "Queso Mascarpone"
                }, 
                profile: "El queso de los postres"
            }, {
                name: "Queso Oaxaca",
                yearsInCompany: 1,
                photo: {
                    "src": "./img/quesito.jpg",
                    "alt": "Queso Oaxaca"
                }, 
                profile: "El queso de las quesadillas en Mexico"
            }*/
        ];
        this.getData(); 
    }

    getData(){
        let xhr = new XMLHttpRequest();
  
        xhr.onload = function()
        {
            if(xhr.status ===200){
                console.log("Peticion completada correctamente");
                let response = JSON.parse(xhr.responseText);
                this.people = response;
            }
        }.bind(this);
        xhr.open("GET", "http://localhost:3000/cheeses");
        xhr.send();
    }

    render() {
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <h2 class="text-center">QUESITOS</h2>
            <div class="row" id="peopleList">
                <div class="row row-cols-1 row-cols-sm-4">
                ${this.people.map(
                    person => html`<queso-ficha-listado 
                                        fname="${person.name}" 
                                        yearsInCompany="${person.age}"
                                        profile="${person.description}"
                                        .photo="${person.image}"
                                        @delete-person="${this.deletePerson}"
                                        @queso-info="${this.infoQueso}"
                                    >
                                </queso-ficha-listado>`
                )}
                </div>
            </div>
            <div class="row">
                  <queso-form id="personForm" class="d-none border rounded border-primary"
	                @persona-form-close="${this.personFormClose}"
                    @persona-form-store="${this.personFormStore}">
		    </queso-form>
	        </div>
            `;
    }

    deletePerson(e) { 
        console.log("deletePerson en persona-main");
        console.log("Se va a borrar la persona de nombre " + e.detail.name);
      
        this.people = this.people.filter(
            person => person.name != e.detail.name
        );
    }
    infoQueso(e) { 
        console.log("InfoQueso en persona-main");
        console.log("Prueba de la muestra de quesos" + e.detail.name);
        this.shadowRoot.querySelector("queso-ficha-listado").showPersonForm = false;
        //this.shadowRoot.getElementById("quesoFichaListado").classList.remove("d-none");
        //this.shadowRoot.getElementById("quesoForm").classList.add("d-none");
           
    }
    updated(changedProperties) { 
        console.log("updated");	
        if (changedProperties.has("showPersonForm")) {
            console.log("Ha cambiado el valor de la propiedad showPersonForm en persona-main");
            //if (this.showPersonForm === true) {
                this.showPersonFormData();
            //} else {
                this.showPersonList();
            //}
        }
    }
    personFormClose() {
        console.log("personFormClose");
        console.log("Se ha cerrado el formulario de la persona");
        this.showPersonForm = false;
    }
    personFormStore(e) {
        console.log("personFormStore");
        console.log("Se va a almacenar una persona");	
                            
        this.people.push(e.detail.person);
      
        console.log("Persona almacenada");	
        this.showPersonForm = false;
    }
    showPersonList() {
        console.log("showPersonList");
        console.log("Mostrando listado de personas");
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
        this.shadowRoot.getElementById("personForm").classList.add("d-none");	  
    }
    
    showPersonFormData() {
        console.log("showPersonFormData");
        console.log("Mostrando formulario de persona");
        this.shadowRoot.getElementById("personForm").classList.remove("d-none");	  
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");	 	  
    }  
}

customElements.define('queso-main', QuesoMain)